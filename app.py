from flask import Flask
from datetime import datetime
import socket

app = Flask(__name__)

@app.route('/')
def hello_world():
    current_time = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
    server_name = socket.gethostname()
    message = f'Hello World! Current time: {current_time}. Server name: {server_name}'
    return message

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8083)

